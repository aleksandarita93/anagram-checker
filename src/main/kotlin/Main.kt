import anagram.exceptions.BlankArgumentException
import anagram.usecases.AnagramCheckRequest
import anagram.usecases.AnagramCheckUseCase

fun main(args: Array<String>) {
    if (args.size != 2) {
        println("Please provide exactly two strings as arguments.")
        return
    }

    val request = AnagramCheckRequest(args[0], args[1])
    val anagramCheckUseCase = AnagramCheckUseCase()

    try {
        if (anagramCheckUseCase.areAnagrams(request)) {
            println("${request.firstWord} and ${request.secondWord} are anagrams.")
        } else {
            println("${request.firstWord} and ${request.secondWord} are not anagrams.")
        }
    } catch (e: BlankArgumentException) {
        println("Please provide non-empty strings as arguments.")
    }
}