package anagram.usecases

data class AnagramCheckRequest(
    val firstWord: String,
    val secondWord: String
)
