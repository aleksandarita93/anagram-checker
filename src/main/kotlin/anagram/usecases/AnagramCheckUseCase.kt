package anagram.usecases

import anagram.exceptions.BlankArgumentException

class AnagramCheckUseCase {
    fun areAnagrams(anagramCheckRequest: AnagramCheckRequest): Boolean {
        assertArgumentsAreNotBlank(anagramCheckRequest)

        val sanitisedFirstWord = sanitiseInput(anagramCheckRequest.firstWord)
        val sanitisedSecondWord = sanitiseInput(anagramCheckRequest.secondWord)

        return sanitisedFirstWord.toCharArray().sorted() == sanitisedSecondWord.toCharArray().sorted()
    }

    private fun assertArgumentsAreNotBlank(anagramCheckRequest: AnagramCheckRequest) {
        if (anagramCheckRequest.firstWord.isBlank() || anagramCheckRequest.secondWord.isBlank()) {
            throw BlankArgumentException()
        }
    }

    private fun sanitiseInput(word1: String): String = word1.lowercase().replace(Regex("[^a-z]"), "")
}