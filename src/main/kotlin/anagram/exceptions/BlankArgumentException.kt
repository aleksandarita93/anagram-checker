package anagram.exceptions

class BlankArgumentException() : IllegalArgumentException()