import anagram.exceptions.BlankArgumentException
import anagram.usecases.AnagramCheckRequest
import anagram.usecases.AnagramCheckUseCase
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Assertions.assertThrows
import org.junit.jupiter.api.Test

class AnagramCheckUseCaseTest {

    private val useCase = AnagramCheckUseCase()

    @Test
    fun `areAnagrams returns true for anagrams`() {
        val request = AnagramCheckRequest("listen", "silent")
        val result = useCase.areAnagrams(request)
        assertEquals(true, result)
    }

    @Test
    fun `areAnagrams returns false for non-anagrams`() {
        val request = AnagramCheckRequest("listen", "talks")
        val result = useCase.areAnagrams(request)
        assertEquals(false, result)
    }

    @Test
    fun `areAnagrams throws BlankArgumentException when either word is blank`() {
        val blankWordRequest1 = AnagramCheckRequest("", "silent")
        val blankWordRequest2 = AnagramCheckRequest("listen", "")

        assertThrows(BlankArgumentException::class.java) {
            useCase.areAnagrams(blankWordRequest1)
        }

        assertThrows(BlankArgumentException::class.java) {
            useCase.areAnagrams(blankWordRequest2)
        }
    }

    @Test
    fun `areAnagrams returns true for anagrams with leading and trailing spaces`() {
        val request = AnagramCheckRequest(" listen ", " silent ")
        val result = useCase.areAnagrams(request)
        assertEquals(true, result)
    }

    @Test
    fun `areAnagrams returns true for anagrams with mixed case`() {
        val request = AnagramCheckRequest("Listen", "Silent")
        val result = useCase.areAnagrams(request)
        assertEquals(true, result)
    }

    @Test
    fun `areAnagrams returns true for anagrams with mixed case and spaces`() {
        val request = AnagramCheckRequest(" LiSten  ", " SIlent ")
        val result = useCase.areAnagrams(request)
        assertEquals(true, result)
    }

    @Test
    fun `areAnagrams returns true for anagrams with special characters`() {
        val request = AnagramCheckRequest("Listen", "Silent!")
        val result = useCase.areAnagrams(request)
        assertEquals(true, result)
    }

    @Test
    fun `areAnagrams returns true for complete sentence anagrams`() {
        val request = AnagramCheckRequest("eleven plus two", "twelve plus one")
        val result = useCase.areAnagrams(request)
        assertEquals(true, result)
    }
}
